---
title: "个人博客开发记录"
subtitle: ""
date: 2022-06-26T14:46:23+08:00
lastmod: 2022-06-26T14:46:23+08:00
draft: false
author: ""
authorLink: ""
description: ""
license: ""
images: []

tags: []
categories: ["技术"]

featuredImage: ""
featuredImagePreview: ""

hiddenFromHomePage: false
hiddenFromSearch: false
twemoji: false
lightgallery: true
ruby: true
fraction: true
fontawesome: true
linkToMarkdown: true
rssFullText: false

toc:
  enable: true
  auto: true
code:
  copy: true
  maxShownLines: 50
math:
  enable: true
  # ...
mapbox:
  # ...
share:
  enable: false
  # ...
comment:
  enable: false
  # ...
library:
  css:
    # someCSS = "some.css"
    # located in "assets/"
    # Or
    # someCSS = "https://cdn.example.com/some.css"
  js:
    # someJS = "some.js"
    # located in "assets/"
    # Or
    # someJS = "https://cdn.example.com/some.js"
seo:
  images: []
  # ...
---

<!--more-->
## 一、开发计划
### 2022
- 6月
  - [x] 立项
  - [x] 前端方案对比&选择：Vue/React$\rightarrow$Vue
  - ~~[x] 前端模板选择：[Fblog](https://gitee.com/fengziy/Fblog)~~
  - [x] 前端项目创建
  - ~~[ ] 后端项目创建~~
- 7月
  - [ ] 学习Vue基础开发
  - [ ] 前端框架搭建
    - [x] 主界面
    - [ ] 按时间排序文章列表界面
    - [ ] 标签列表界面
    - [ ] 分类列表界面
    - [ ] 关于界面
  - [ ] 后端项目创建
  - ~~[ ] 后端框架搭建~~
- 8 月
  - [ ] 文章内容展示界面
  - [ ] 搜索功能
  - [ ] 后端功能接口
  - [ ] 后端管理界面
## 二、开发日志
### 2022年
#### 6月
-     26日：立项
-     27日：创建了前端Vue项目
-     29日：导航栏样式+Footer
-     30日：主页
#### 7月
-     1日：主页界面+路由
-     5日：导航栏跳转界面
-     11日：文章列表

## 三、功能规划

目前计划从零开始，不依赖任何模板，先完成一个只包含最基本功能的版本并发布，在此基础上进行迭代，以下为希望最终实现的功能（更新中）：

### 1 界面功能
网站规划由以下几个部分组成：

#### 1.1 通用布局

- 顶部导航栏功能TODO
  - [x] 标题/图标
  - [x] 搜索
  - [x] 文章
  - [x] 标签
  - [x] 分类
  - [x] 关于
- 底部展示版权备案相关信息

#### 1.1 首页
展示以下通用信息：
- [x] 主页描述
- [x] 最近三篇博客：标题/摘要/时间/标签/类别
- [ ] 按博客时间访问的链接列表
- [ ] 按标签访问的链接列表
- [ ] 按类别访问的链接列表

#### 1.2 文章归档
- [ ] 展示按时间顺序的文章列表
- [ ] 按年月形成树形结构

#### 1.3 按标签查找
- [ ] 展示标签信息
- [ ] 按对应文章数设置标签大小/排序
- [ ] 按字母序排序

#### 1.4 按类别查找
- [ ] 展示按类别的文章列表
- [ ] 按字母序排序

### 2 发文管理
#### 2.1 文章格式
- [ ] 自己手写原始html
- [ ] markdown自动生成页面

### 3 部署方案
- [x] 云服务器：618新人折扣的百度轻量应用服务器
- [ ] 域名备案：两年域名：wang-jh.com，尚未备案
- [ ] Github CI/CD

## 四、技术方案
### 1. 前端
- Vue
  - [ ] 后端保存markdown文件，由前端负责渲染
- Nginx部署

### 2. 后端
- [ ] Asp.Net Core 6
- [ ] PostgreSql
